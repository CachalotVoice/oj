package cn.cachalot.oj.schedule;

import cn.cachalot.oj.service.CopyCheckService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Slf4j
@Component
@Async
public class CopyCheckSchedule {
    @Resource
    CopyCheckService copyCheckService;

    @Scheduled(cron = "0 0 4 * * ?")
    public void copyCheck() {
        copyCheckService.checkAll();
    }
}
