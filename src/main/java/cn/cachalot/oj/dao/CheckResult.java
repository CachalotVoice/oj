package cn.cachalot.oj.dao;

import cn.cachalot.oj.entity.Submit;
import lombok.Data;

@Data
public class CheckResult {
    Boolean isCopied;
    Double similarRate;
    Long copiedSubmitId;
    Submit currentSubmit;
}
