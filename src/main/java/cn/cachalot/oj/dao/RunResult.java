package cn.cachalot.oj.dao;

import lombok.Data;

@Data
public class RunResult {
    private Boolean timeOut;
    private String output;
    private String errorOutput;
}
