package cn.cachalot.oj.dao;

import lombok.Data;

@Data
public class CompileResult {
    private String output;
    private String errorOutput;
}
