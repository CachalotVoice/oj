package cn.cachalot.oj;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@Slf4j
@SpringBootApplication
@ServletComponentScan
@MapperScan("cn.cachalot.oj.mapper")
@EnableScheduling //开启定时任务
@EnableAsync
public class OjApplication {

    public static void main(String[] args) {
        SpringApplication.run(OjApplication.class, args);
        log.info("项目启动成功!");
    }

}
