package cn.cachalot.oj.entity;

import lombok.Data;

import java.io.Serializable;
@Data
public class ProblemDetail implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private String description;
    private String input;
    private String output;
    private String exampleInput;
    private String exampleOutput;
}
