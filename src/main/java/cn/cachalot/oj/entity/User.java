package cn.cachalot.oj.entity;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private Integer level;
    private String userName;
    private String password;
    private LocalDateTime createTime;
}
