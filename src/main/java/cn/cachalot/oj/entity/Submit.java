package cn.cachalot.oj.entity;

import io.swagger.models.auth.In;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
@Data
public class Submit implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private Long problemId;
    private Long userId;
    private Integer language;
    private String code;
    private LocalDateTime submitTime;
    private Integer status;
}
