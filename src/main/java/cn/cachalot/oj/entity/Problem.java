package cn.cachalot.oj.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Problem implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private String title;
    private Integer difficulty;
    private String label;
}
