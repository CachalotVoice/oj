package cn.cachalot.oj.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Solution implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private Long problemId;
    private Integer language;
    private String code;

}
