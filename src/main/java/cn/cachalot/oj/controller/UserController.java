package cn.cachalot.oj.controller;

import cn.cachalot.oj.controller.param.LoginParam;
import cn.cachalot.oj.controller.param.RegisterParam;
import cn.cachalot.oj.controller.param.UpdateUserInfoParam;
import cn.cachalot.oj.controller.res.*;
import cn.cachalot.oj.entity.User;
import cn.cachalot.oj.service.ProblemService;
import cn.cachalot.oj.service.UserService;
import cn.cachalot.oj.util.BaseContext;
import cn.cachalot.oj.util.R;
import cn.cachalot.oj.util.TokenUtil;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Slf4j
@RestController
@Api(tags = "用户信息")
public class UserController {
    @Resource
    private UserService userService;
    @Resource
    private ProblemService problemService;

    @PostMapping("/login")
    @ApiOperation("用户登陆")
    @ApiImplicitParam(name = "loginParam", value = "登陆信息", required = true)
    public R<LoginRes> login(@RequestBody LoginParam loginParam) {
        String username = loginParam.getUserName();
        String password = loginParam.getPassword();
        if (username == null || password == null || username.equals("") || password.equals("")) {
            return R.error("用户名或密码不能为空!");
        }
        User user = userService.login(username, password);
        if (user == null) {
            return R.error("用户名或密码错误!");
        }
        password = DigestUtils.md5DigestAsHex(password.getBytes());
        if (!user.getPassword().equals(password)) {
            return R.error("用户名或密码错误!");
        }
        String token = TokenUtil.creatToken(user);
        LoginRes loginRes = new LoginRes();
        loginRes.setToken(token);
        loginRes.setLevel(user.getLevel());
        loginRes.setUserID(user.getId());
        return R.success("登陆成功!").data(loginRes);
    }

    @PostMapping("/register")
    @ApiOperation("用户注册")
    @ApiImplicitParam(name = "registerParam", value = "注册信息", required = true)
    public R register(@RequestBody RegisterParam registerParam) {
        String isSuccess = userService.register(registerParam);
        if (!isSuccess.equals("success")) {
            return R.error(isSuccess);
        }
        return R.success("注册成功!");
    }

    @GetMapping("/user/userList")
    @ApiOperation("获取用户列表")
    @ApiImplicitParams({@ApiImplicitParam(name = "pageNum", value = "第几页", required = true),
            @ApiImplicitParam(name = "pageSize", value = "每一页大小", required = true)})
    public R<PageInfo<UserInfoRes>> getUserList(@RequestParam Integer pageNum, @RequestParam Integer pageSize) {
        if (BaseContext.getLevel() < 1) {
            return R.error("权限不足!");
        }
        if (pageNum == null || pageSize == null) {
            return R.error("缺少参数!");
        }
        List<UserInfoRes> list = userService.getUserList(pageNum, pageSize);
        if (list == null) {
            list = new ArrayList<>();
        }
        PageInfo<UserInfoRes> pageInfo = new PageInfo<>(list);
        return R.success("用户列表").data(pageInfo);
    }

    @PutMapping("/user/updateUserInfo")
    @ApiOperation("修改用户信息")
    @ApiImplicitParam(name = "updateUserInfoParam", value = "修改用户信息", required = true)
    public R<String> updateTestcase(@RequestBody UpdateUserInfoParam updateUserInfoParam) {
        String isSuccess = userService.updateUserInfo(updateUserInfoParam);
        if (!Objects.equals(isSuccess, "success")) {
            return R.error(isSuccess);
        }
        return R.success("修改成功!");
    }

    @GetMapping("/submit/list")
    @ApiOperation("获取历史提交记录列表")
    @ApiImplicitParams(
            {@ApiImplicitParam(name = "userId", value = "指定用户Id(为0表示查询所有人记录)", required = true),
                    @ApiImplicitParam(name = "pageNum", value = "第几页", required = true),
                    @ApiImplicitParam(name = "pageSize", value = "每一页大小", required = true)})
    public R<PageInfo<SubmitInfoRes>> getSubmitInfoRes(@RequestParam Long userId, @RequestParam Integer pageNum,
                                                       @RequestParam Integer pageSize) {
        List<SubmitInfoRes> submitInfoResList = problemService.getSubmitInfoResListByUserId(userId, pageNum, pageSize);
        if (submitInfoResList == null) {
            return R.error("找不到提交记录!");
        }
        return R.success("提交记录").data(submitInfoResList);
    }

    @GetMapping("/submit/detail")
    @ApiOperation("获取历史提交信息")
    @ApiImplicitParam(name = "submitId", value = "提交Id", required = true)
    public R<SubmitInfoDetailRes> getSubmitInfoDetail(@RequestParam Long submitId) {
        return problemService.getSubmitInfoDetailResBySubmitId(submitId);
    }

    @GetMapping("/user/status")
    @ApiOperation("获取用户做题数据")
    @ApiImplicitParam(name = "userId", value = "用户id", required = true)
    public R<UserStatusRes> getUserStatus(@RequestParam Long userId) {
        return userService.getUserStatusByUserId(userId);
    }
}









