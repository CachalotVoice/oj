package cn.cachalot.oj.controller.res;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(description = "测试用例返回值")
public class TestCaseInfoRes implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "测试用例id", required = true)
    private Long id;
    @ApiModelProperty(value = "输入", required = true)
    private String input;
    @ApiModelProperty(value = "输出", required = true)
    private String output;

}
