package cn.cachalot.oj.controller.res;

import com.sun.jdi.event.StepEvent;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@ApiModel(description = "历史提交结果")
public class SubmitInfoRes implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "提交id", required = true)
    private Long id;
    @ApiModelProperty(value = "问题id", required = true)
    private Long problemId;
    @ApiModelProperty(value = "对应问题的标题", required = true)
    private String title;
    @ApiModelProperty(value = "题目标签类型", required = true)
    private String label;
    @ApiModelProperty(value = "提交者Id", required = true)
    private Long userId;
    @ApiModelProperty(value = "提交者姓名", required = true)
    private String userName;
    @ApiModelProperty(value = "提交时间", required = true)
    private LocalDateTime submitTime;
    @ApiModelProperty(value = "状态,0-等待,1-运行通过,等待抄袭检测,2-编译出错,3-运行出错,4-答案错误,5-判定抄袭,6-通过且未抄袭", required = true)
    private Integer status;

}
