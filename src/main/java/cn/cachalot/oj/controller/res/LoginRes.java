package cn.cachalot.oj.controller.res;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(description = "登陆返回")
public class LoginRes implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "token", required = true)
    private String token;
    @ApiModelProperty(value = "用户权限,0-普通用户,1-管理员", required = true)
    private Integer level;
    @ApiModelProperty(value = "用户ID", required = true)
    private Long userID;
}
