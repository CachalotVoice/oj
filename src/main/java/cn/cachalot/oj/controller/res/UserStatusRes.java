package cn.cachalot.oj.controller.res;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@ApiModel(description = "用户统计数据")
public class UserStatusRes implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "用户id", required = true)
    private Long id;
    @ApiModelProperty(value = "权限,0-普通人,1-管理员", required = true)
    private Integer level;
    @ApiModelProperty(value = "用户名", required = true)
    private String userName;
    @ApiModelProperty(value = "创建时间", required = true)
    private LocalDateTime createTime;
    @ApiModelProperty(value = "提交次数", required = true)
    private Integer submitCount;
    @ApiModelProperty(value = "已解决的题目", required = true)
    private Integer resolvedCount;
}
