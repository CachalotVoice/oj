package cn.cachalot.oj.controller.res;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@ApiModel(description = "用户信息返回值")
public class UserInfoRes implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "用户id", required = true)
    private Long id;
    @ApiModelProperty(value = "权限,0-普通人,1-管理员", required = true)
    private Integer level;
    @ApiModelProperty(value = "用户名", required = true)
    private String userName;
    @ApiModelProperty(value = "创建时间", required = true)
    private LocalDateTime createTime;
}
