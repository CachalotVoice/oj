package cn.cachalot.oj.controller.res;

import com.sun.jdi.event.StepEvent;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(description = "题目的基本信息")
public class QuestionInfoRes implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "题目id", required = true)
    private Long id;
    @ApiModelProperty(value = "题目标题", required = true)
    private String title;
    @ApiModelProperty(value = "题目标签类型", required = true)
    private String label;
    @ApiModelProperty(value = "题目难度:0-简单,1-中等,2-困难", required = true)
    private Integer difficulty;
    @ApiModelProperty(value = "是否已解决0-否,1-是", required = true)
    private Boolean isResolved;
    @ApiModelProperty(value = "通过率", required = true)
    private Double passRate;
}
