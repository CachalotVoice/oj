package cn.cachalot.oj.controller.res;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@ApiModel(description = "抄袭检测返回值")
public class CopyCheckRes implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "提交id", required = true)
    private Long submitId;
    @ApiModelProperty(value = "抄袭者id", required = true)
    private Long userId;
    @ApiModelProperty(value = "抄袭者名字", required = true)
    private String userName;
    @ApiModelProperty(value = "问题id", required = true)
    private Long problemId;
    @ApiModelProperty(value = "抄袭代码", required = true)
    private String code;
    @ApiModelProperty(value = "被抄袭代码", required = true)
    private String copiedCode;
    @ApiModelProperty(value = "相似度", required = true)
    private Double similarRate;
    @ApiModelProperty(value = "抄袭代码提交时间", required = true)
    private LocalDateTime submitTime;
    @ApiModelProperty(value = "被抄袭代码提交时间", required = true)
    private LocalDateTime copiedSubmitTime;
}
