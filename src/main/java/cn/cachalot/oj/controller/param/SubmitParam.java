package cn.cachalot.oj.controller.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(description = "提交代码入参")
public class SubmitParam implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "题目Id", required = true)
    private Long id;
    @ApiModelProperty(value = "提交的语言0-java,1-C/C++,2-python,3-javascript", required = true)
    private Integer language;
    @ApiModelProperty(value = "提交的代码内容", required = true)
    private String code;
}
