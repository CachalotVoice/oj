package cn.cachalot.oj.controller.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(description = "修改用户信息入参")
public class UpdateUserInfoParam implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "用户id", required = true)
    private Long id;
    @ApiModelProperty(value = "用户名", required = false)
    private String userName;
    @ApiModelProperty(value = "权限,0-普通人,1-管理员", required = false)
    private Integer level;
    @ApiModelProperty(value = "密码", required = false)
    private String password;
}
