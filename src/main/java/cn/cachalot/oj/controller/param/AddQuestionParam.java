package cn.cachalot.oj.controller.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(description = "添加题目入参")
public class AddQuestionParam implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "题目标题", required = true)
    private String title;
    @ApiModelProperty(value = "题目标签类型", required = true)
    private String label;
    @ApiModelProperty(value = "题目难度:0-简单,1-中等,2-困难", required = true)
    private Integer difficulty;
    @ApiModelProperty(value = "题目内容描述", required = true)
    private String description;
    @ApiModelProperty(value = "输入参数说明", required = true)
    private String input;
    @ApiModelProperty(value = "输出参数说明", required = true)
    private String output;
    @ApiModelProperty(value = "输入实例", required = true)
    private String exampleInput;
    @ApiModelProperty(value = "输出实例", required = true)
    private String exampleOutput;
}
