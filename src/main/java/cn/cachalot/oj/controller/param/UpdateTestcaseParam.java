package cn.cachalot.oj.controller.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(description = "修改测试用例入参")
public class UpdateTestcaseParam implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "对应测试用例Id", required = true)
    private Long id;
    @ApiModelProperty(value = "输入参数", required = false)
    private String input;
    @ApiModelProperty(value = "输出参数", required = false)
    private String output;
}
