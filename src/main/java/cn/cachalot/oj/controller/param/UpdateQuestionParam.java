package cn.cachalot.oj.controller.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(description = "修改问题信息入参")
public class UpdateQuestionParam implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "题目id", required = true)
    private String id;
    @ApiModelProperty(value = "题目标题", required = false)
    private String title;
    @ApiModelProperty(value = "题目类型标签", required = false)
    private String label;
    @ApiModelProperty(value = "题目难度:0-简单,1-中等,2-困难", required = false)
    private Integer difficulty;
    @ApiModelProperty(value = "题目内容描述", required = false)
    private String description;
    @ApiModelProperty(value = "输入参数说明", required = false)
    private String input;
    @ApiModelProperty(value = "输出参数说明", required = false)
    private String output;
    @ApiModelProperty(value = "输入实例", required = false)
    private String exampleInput;
    @ApiModelProperty(value = "输出实例", required = false)
    private String exampleOutput;

}
