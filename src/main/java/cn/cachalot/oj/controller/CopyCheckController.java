package cn.cachalot.oj.controller;

import cn.cachalot.oj.controller.res.CopyCheckRes;
import cn.cachalot.oj.controller.res.QuestionInfoRes;
import cn.cachalot.oj.service.CopyCheckService;
import cn.cachalot.oj.util.BaseContext;
import cn.cachalot.oj.util.R;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/copyCheck")
@Api(tags = "抄袭检测相关接口")
public class CopyCheckController {
    @Resource
    CopyCheckService copyCheckService;

    @GetMapping("/checkAll")
    @ApiOperation("检测所有未检测代码")
    public R<List<CopyCheckRes>> getAllQuestion() {
        if (BaseContext.getLevel() < 1) {
            return R.error("权限不足!");
        }
        List<CopyCheckRes> list = copyCheckService.getCheckAllCopyCheckResList();
        return R.success("检测结果").data(list);
    }
}
