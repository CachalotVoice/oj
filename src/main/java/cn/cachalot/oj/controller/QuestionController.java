package cn.cachalot.oj.controller;

import cn.cachalot.oj.controller.param.*;
import cn.cachalot.oj.controller.res.QuestionDetailInfoRes;
import cn.cachalot.oj.controller.res.QuestionInfoRes;
import cn.cachalot.oj.controller.res.TestCaseInfoRes;
import cn.cachalot.oj.entity.Submit;
import cn.cachalot.oj.service.ProblemService;
import cn.cachalot.oj.service.SubmitService;
import cn.cachalot.oj.util.BaseContext;
import cn.cachalot.oj.util.R;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Slf4j
@RestController
@RequestMapping("/question")
@Api(tags = "题目管理相关接口")
public class QuestionController {
    @Resource
    private ProblemService problemService;
    @Resource
    private SubmitService submitService;

    @GetMapping("/questionList")
    @ApiOperation("获取题目列表")
    @ApiImplicitParams({@ApiImplicitParam(name = "pageNum", value = "第几页", required = true),
            @ApiImplicitParam(name = "pageSize", value = "每一页大小", required = true)})
    public R<PageInfo<QuestionInfoRes>> getAllQuestion(@RequestParam Integer pageNum, @RequestParam Integer pageSize) {
        if (pageNum == null || pageSize == null) {
            return R.error("缺少参数!");
        }
        List<QuestionInfoRes> list = problemService.getQuestionInfoResList(pageNum, pageSize);
        if (list == null) {
            list = new ArrayList<>();
        }
        PageInfo<QuestionInfoRes> pageInfo = new PageInfo<>(list);
        return R.success("题目列表").data(pageInfo);
    }

    @GetMapping("/detail")
    @ApiOperation("题目详细信息")
    @ApiImplicitParam(name = "questionId", value = "指定题目的Id", required = true)
    public R<QuestionDetailInfoRes> getDetailInfo(@RequestParam Long questionId) {
        QuestionDetailInfoRes questionDetailInfoRes = problemService.getQuestionDetailInfoResByquestionId(questionId);
        if (questionDetailInfoRes == null) {
            return R.error("找不到题目信息!");
        }
        return R.success("题目详细信息").data(questionDetailInfoRes);
    }

    @PostMapping("/submit")
    @ApiOperation("提交代码")
    @ApiImplicitParam(name = "submitParam", value = "提交代码入参", required = true)
    public R<String> submit(@RequestBody SubmitParam submitParam) throws IOException, InterruptedException {

        R<Submit> submitR = submitService.submit(submitParam);
        if (!submitR.getSuccess()) {
            return R.error("提交失败!");
        }
        return submitService.compileAndRun(submitR.getData());
    }

    @PostMapping("/addQuestion")
    @ApiOperation("添加题目")
    @ApiImplicitParam(name = "addQuestionParam", value = "添加题目入参", required = true)
    public R<String> addQuestion(@RequestBody AddQuestionParam addQuestionParam) {
        if (BaseContext.getLevel() < 1) {
            return R.error("权限不足!");
        }
        String isSuccess = problemService.addQuestion(addQuestionParam);
        if (!Objects.equals(isSuccess, "success")) {
            return R.error(isSuccess);
        }
        return R.success("添加成功!");
    }

    @PostMapping("/addTestCase")
    @ApiOperation("添加测试用例")
    @ApiImplicitParam(name = "addTestCaseParam", value = "添加测试用例入参", required = true)
    public R<String> addTestCase(@RequestBody AddTestCaseParam addTestCaseParam) {
        if (BaseContext.getLevel() < 1) {
            return R.error("权限不足!");
        }
        String isSuccess = problemService.addTestCase(addTestCaseParam);
        if (!Objects.equals(isSuccess, "success")) {
            return R.error(isSuccess);
        }
        return R.success("添加成功!");
    }

    @GetMapping("/testCaseList")
    @ApiOperation("获取指定题目的测试用例列表")
    @ApiImplicitParams({@ApiImplicitParam(name = "questionId", value = "指定题目的Id", required = true),
            @ApiImplicitParam(name = "pageNum", value = "第几页", required = true),
            @ApiImplicitParam(name = "pageSize", value = "每一页大小", required = true)})
    public R<PageInfo<TestCaseInfoRes>> getTestCase(@RequestParam Long questionId, @RequestParam Integer pageNum,
                                                    @RequestParam Integer pageSize) {
        if (BaseContext.getLevel() < 1) {
            return R.error("权限不足!");
        }
        if (pageNum == null || pageSize == null) {
            return R.error("缺少参数!");
        }
        List<TestCaseInfoRes> list = problemService.getProblemCheckListByProblemId(questionId, pageNum, pageSize);
        if (list == null) {
            list = new ArrayList<>();
        }
        PageInfo<TestCaseInfoRes> pageInfo = new PageInfo<>(list);
        return R.success("题目列表").data(pageInfo);
    }

    @DeleteMapping("/deleteQuestion")
    @ApiOperation("删除题目")
    @ApiImplicitParam(name = "questionId", value = "题目Id", required = true)
    public R deleteQuestion(@RequestParam Long questionId) {
        if (BaseContext.getLevel() < 1) {
            return R.error("权限不足!");
        }
        String isSuccess = problemService.deleteProblem(questionId);
        if (!isSuccess.equals("success")) {
            return R.error(isSuccess);
        }
        return R.success("删除成功!");
    }

    @DeleteMapping("/deleteTestCase")
    @ApiOperation("删除测试用例")
    @ApiImplicitParam(name = "testCaseId", value = "测试用例ID", required = true)
    public R deleteTestCase(@RequestParam Long testCaseId) {
        if (BaseContext.getLevel() < 1) {
            return R.error("权限不足!");
        }
        String isSuccess = problemService.deleteTestCaseByTestCaseId(testCaseId);
        if (!isSuccess.equals("success")) {
            return R.error(isSuccess);
        }
        return R.success("删除成功!");
    }

    @PutMapping("/updateQuestion")
    @ApiOperation("修改题目信息")
    @ApiImplicitParam(name = "updateQuestionParam", value = "修改题目信息入参", required = true)
    public R<String> updateQuestion(@RequestBody UpdateQuestionParam updateQuestionParam) {
        if (BaseContext.getLevel() < 1) {
            return R.error("权限不足!");
        }
        String isSuccess = problemService.updateQuestion(updateQuestionParam);
        if (!Objects.equals(isSuccess, "success")) {
            return R.error(isSuccess);
        }
        return R.success("添加成功!");
    }

    @PutMapping("/updateTestcase")
    @ApiOperation("修改测试用例")
    @ApiImplicitParam(name = "updateTestcaseParam", value = "修改测试用例入参", required = true)
    public R<String> updateTestcase(@RequestBody UpdateTestcaseParam updateTestcaseParam) {
        if (BaseContext.getLevel() < 1) {
            return R.error("权限不足!");
        }
        String isSuccess = problemService.updateTestcase(updateTestcaseParam);
        if (!Objects.equals(isSuccess, "success")) {
            return R.error(isSuccess);
        }
        return R.success("添加成功!");
    }


}









