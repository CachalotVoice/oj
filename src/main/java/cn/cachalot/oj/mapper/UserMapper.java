package cn.cachalot.oj.mapper;

import cn.cachalot.oj.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserMapper extends BaseMapper<User> {
    User getUserByUserName(@Param("userName") String userName);

    List<User> getUserList();

    String getUserNameByUserId(@Param("userId") Long userId);
}
