package cn.cachalot.oj.mapper;

import cn.cachalot.oj.entity.Solution;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SolutionMapper extends BaseMapper<Solution> {
    void deleteAllByProblemId(@Param("problemId") Long problemId);

    List<Solution> getSolutionListByProblemIdAndLanguage(@Param("problemId") Long problemId,
                                                         @Param("language") Integer language);
}
