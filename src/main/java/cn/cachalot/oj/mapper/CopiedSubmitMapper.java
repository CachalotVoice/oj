package cn.cachalot.oj.mapper;

import cn.cachalot.oj.entity.CopiedSubmit;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CopiedSubmitMapper extends BaseMapper<CopiedSubmit> {
}
