package cn.cachalot.oj.mapper;

import cn.cachalot.oj.entity.Problem;
import cn.cachalot.oj.entity.ProblemCheck;
import cn.cachalot.oj.entity.ProblemDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ProblemMapper extends BaseMapper<Problem> {

    List<Problem> getAll();

    Problem getProblemByProblemId(@Param("questionId") Long questionId);

    ProblemDetail getProblemDetailByProblemId(@Param("questionId") Long questionId);

    String getTitleByProblemId(@Param("problemId") Long problemId);

    Integer getResolvedCountByProblemIdAndUserId(@Param("id") Long id, @Param("userId") Long userId);

    Integer getSubmitCountByUserId(@Param("userId") Long userId);

    Integer getResolvedCountByUserId(@Param("userId") Long userId);

    Double getResolvedCountByProblemId(@Param("problemId") Long problemId);

    Double getSubmitCountByProblemId(@Param("problemId") Long problemId);
}
