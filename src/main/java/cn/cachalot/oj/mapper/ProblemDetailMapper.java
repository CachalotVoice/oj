package cn.cachalot.oj.mapper;

import cn.cachalot.oj.entity.ProblemDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ProblemDetailMapper extends BaseMapper<ProblemDetail> {

}
