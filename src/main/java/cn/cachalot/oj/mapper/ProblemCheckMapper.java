package cn.cachalot.oj.mapper;

import cn.cachalot.oj.entity.ProblemCheck;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ProblemCheckMapper extends BaseMapper<ProblemCheck> {
    List<ProblemCheck> getProblemCheckListByProblemId(@Param("problemId") Long problemId);

    void deleteAllByProblemId(@Param("problemId") Long problemId);
}
