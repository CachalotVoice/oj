package cn.cachalot.oj.mapper;

import cn.cachalot.oj.entity.Submit;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SubmitMapper extends BaseMapper<Submit> {
    void deleteAllByProblemId(@Param("problemId") Long problemId);

    List<Submit> getSubmitList();

    List<Submit> getSubmitListByUserId(@Param("userId") Long userId);

    List<Submit> getUncheckedSubmitList();

    Long getUserIdBySubmitId(@Param("id") Long id);
}
