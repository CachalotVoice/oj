package cn.cachalot.oj.util;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Slf4j
@Component
public class CheckCopy {
    public Double check(String currentCode, String code) throws Exception {
        ClassPathResource classPathResource = new ClassPathResource("\\checkCopy");
        String classPathStr = classPathResource.getFile().getAbsolutePath();
        File file = new File(classPathStr + "\\code1.txt");
        if (!file.exists()) {
            file.createNewFile();
        }
        FileWriter fileWritter = new FileWriter(file.getName(), false);
        fileWritter.write(currentCode);
        fileWritter.close();
        file = new File(classPathStr + "\\code2.txt");
        if (!file.exists()) {
            file.createNewFile();
        }
        fileWritter = new FileWriter(file.getName(), false);
        fileWritter.write(code);
        fileWritter.close();
        String[] arg = new String[]{"python3", classPathStr+"\\check.py"};
        Process proc;
        proc = Runtime.getRuntime().exec(arg);
        StringBuilder outputBuilder = new StringBuilder();
        InputStream stdoutFrom = proc.getInputStream();
        while (true) {
            int ch = stdoutFrom.read();
            if (ch == -1) {
                break;
            }
            outputBuilder.append((char) ch);
        }

        stdoutFrom.close();
        return Double.valueOf(outputBuilder.toString());
    }
}
