package cn.cachalot.oj.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class ProcessOutputThread extends Thread {
    private InputStream is;
    private String output;

    public ProcessOutputThread(InputStream is) throws IOException {
        if (null == is) {
            throw new IOException("the provided InputStream is null");
        }
        this.is = is;
        this.output = "";
    }

    public String getOutput() {
        return this.output;
    }

    @Override
    public void run() {
        StringBuilder outputBuilder = new StringBuilder();
        InputStream stdoutFrom = this.is;
        while (true) {
            int ch = 0;
            try {
                ch = stdoutFrom.read();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            if (ch == -1) {
                break;
            }
            outputBuilder.append((char) ch);
        }
        try {
            stdoutFrom.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        this.output = outputBuilder.toString();
    }
}