package cn.cachalot.oj.util;

import lombok.extern.slf4j.Slf4j;

/**
 * 存储当前线程信息
 */
@Slf4j
public class BaseContext {
    private static ThreadLocal<Long> threadLocalId = new ThreadLocal<>();
    private static ThreadLocal<Integer> threadLocalLevel = new ThreadLocal<>();

    public static void setId(Long id) {
        threadLocalId.set(id);
    }

    public static Long getId() {
        return threadLocalId.get();
    }

    public static void setLevel(Integer type) {
        threadLocalLevel.set(type);
    }

    public static Integer getLevel() {
        return threadLocalLevel.get();
    }
}
