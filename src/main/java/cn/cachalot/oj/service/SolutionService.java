package cn.cachalot.oj.service;

import cn.cachalot.oj.entity.Solution;
import com.baomidou.mybatisplus.extension.service.IService;

public interface SolutionService extends IService<Solution> {
}
