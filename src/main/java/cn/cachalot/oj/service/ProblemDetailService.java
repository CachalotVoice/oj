package cn.cachalot.oj.service;

import cn.cachalot.oj.entity.ProblemDetail;
import com.baomidou.mybatisplus.extension.service.IService;

public interface ProblemDetailService extends IService<ProblemDetail> {
}
