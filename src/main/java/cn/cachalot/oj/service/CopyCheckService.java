package cn.cachalot.oj.service;

import cn.cachalot.oj.controller.res.CopyCheckRes;
import cn.cachalot.oj.dao.CheckResult;
import cn.cachalot.oj.entity.Submit;

import java.util.List;

public interface CopyCheckService {
    CheckResult checkBySubmit(Submit submit);

    List<CheckResult> checkAll();

    List<CopyCheckRes> getCheckAllCopyCheckResList();
}
