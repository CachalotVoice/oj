package cn.cachalot.oj.service;

import cn.cachalot.oj.controller.param.AddQuestionParam;
import cn.cachalot.oj.controller.param.AddTestCaseParam;
import cn.cachalot.oj.controller.param.UpdateQuestionParam;
import cn.cachalot.oj.controller.param.UpdateTestcaseParam;
import cn.cachalot.oj.controller.res.*;
import cn.cachalot.oj.entity.Problem;
import cn.cachalot.oj.util.R;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface ProblemService extends IService<Problem> {
    List<QuestionInfoRes> getQuestionInfoResList(Integer pageNum, Integer pageSize);

    QuestionDetailInfoRes getQuestionDetailInfoResByquestionId(Long questionId);

    String addQuestion(AddQuestionParam addQuestionParam);

    String addTestCase(AddTestCaseParam addTestCaseParam);

    List<TestCaseInfoRes> getProblemCheckListByProblemId(Long questionId, Integer pageNum, Integer pageSize);

    String deleteProblem(Long questionId);

    String deleteTestCaseByTestCaseId(Long testCaseId);

    String updateQuestion(UpdateQuestionParam updateQuestionParam);

    String updateTestcase(UpdateTestcaseParam updateTestcaseParam);

    List<SubmitInfoRes> getSubmitInfoResListByUserId(Long userId, Integer pageNum, Integer pageSize);

    R<SubmitInfoDetailRes> getSubmitInfoDetailResBySubmitId(Long submitId);


}
