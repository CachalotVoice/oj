package cn.cachalot.oj.service;

import cn.cachalot.oj.controller.param.SubmitParam;
import cn.cachalot.oj.entity.Submit;
import cn.cachalot.oj.util.R;
import com.baomidou.mybatisplus.extension.service.IService;

import java.io.IOException;

public interface SubmitService extends IService<Submit> {
    R<Submit> submit(SubmitParam submitParam);

    R<String> compileAndRun(Submit submit) throws IOException, InterruptedException;
}
