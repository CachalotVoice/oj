package cn.cachalot.oj.service.impl;

import cn.cachalot.oj.entity.ProblemDetail;
import cn.cachalot.oj.mapper.ProblemDetailMapper;
import cn.cachalot.oj.service.ProblemDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class ProblemDetailServiceImpl extends ServiceImpl<ProblemDetailMapper, ProblemDetail>
        implements ProblemDetailService {
}
