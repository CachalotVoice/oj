package cn.cachalot.oj.service.impl;

import cn.cachalot.oj.entity.ProblemCheck;
import cn.cachalot.oj.mapper.ProblemCheckMapper;
import cn.cachalot.oj.service.ProblemCheckService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class ProblemCheckServiceImpl extends ServiceImpl<ProblemCheckMapper, ProblemCheck>
        implements ProblemCheckService {
}
