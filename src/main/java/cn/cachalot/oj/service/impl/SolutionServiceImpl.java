package cn.cachalot.oj.service.impl;

import cn.cachalot.oj.entity.Solution;
import cn.cachalot.oj.mapper.SolutionMapper;
import cn.cachalot.oj.service.SolutionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class SolutionServiceImpl extends ServiceImpl<SolutionMapper, Solution> implements SolutionService {
}
