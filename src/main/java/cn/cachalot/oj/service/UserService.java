package cn.cachalot.oj.service;

import cn.cachalot.oj.controller.param.RegisterParam;
import cn.cachalot.oj.controller.param.UpdateUserInfoParam;
import cn.cachalot.oj.controller.res.UserInfoRes;
import cn.cachalot.oj.controller.res.UserStatusRes;
import cn.cachalot.oj.entity.User;
import cn.cachalot.oj.util.R;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface UserService extends IService<User> {

    User login(String username, String password);

    String register(RegisterParam registerParam);

    List<UserInfoRes> getUserList(Integer pageNum, Integer pageSize);

    String updateUserInfo(UpdateUserInfoParam updateUserInfoParam);

    R<UserStatusRes> getUserStatusByUserId(Long userId);

    String getUserNameById(Long userId);
}
