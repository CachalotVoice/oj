package cn.cachalot.oj.service;

import cn.cachalot.oj.entity.ProblemCheck;
import com.baomidou.mybatisplus.extension.service.IService;

public interface ProblemCheckService extends IService<ProblemCheck> {
}
